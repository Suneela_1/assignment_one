
"use strict";
exports.__esModule = true;

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userSchema = new mongoose.Schema({
    User_name: {
        type: String,
        validate: {
            validator: function (name) {
                return name.length > 3
            }
        },
        required: true,
        set: convert_lower
    },

    Contact_number:{
        type: Number,
        validate: {
            validator: function (contact) {
                return contact.length > 9 && contact.length < 11
            }
        },
        required: true
    },

    Country_name:{
        type: String,
        validate: {
            validator: function (country){
                return country.length > 2
            }
        },
        required: true
    },

    Password:{
        type: String,
        validate: {
            validator: function (password) {
                const pswd1 = /[a-z]/;
                const pswd2 = /[A-Z]/;
                const pswd3 = /[0-9]/;
                const psd1 = pswd1.test(password);
                const psd2 = pswd2.test(password);
                const psd3 = pswd3.test(password);
                return psd1 && psd2 && psd3 && password.length > 8 && password.length <12
            },
            message: "Password length should be 8 to 12 characters long with uppercase, lowercase letters and numbers"
        },
        required: true
    }
});

function convert_lower(User_name){
    if(!(this instanceof mongoose.Document)){
        return User_name
    }
    return User_name.toLowerCase();
}

userSchema.pre('validate', function (next) {
    next();
});

exports.User = mongoose.model('User', userSchema);