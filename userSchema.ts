
import * as mongoose from 'mongoose';
import  {IUser} from './User'
const Schema = mongoose.Schema;

const userSchema : mongoose.Schema<IUser> = new mongoose.Schema<IUser>({
    User_name: {
        type: String,
        validate: {
            validator: function (name: string): boolean {
                return name.length > 3
            }
        },
        required: true,
        unique: true,
        set: convert_lower
    },

    Contact_number:{
        type: Number,
        validate: {
            validator: function (contact: number): boolean {
                return contact > 999999999 && contact < 9999999999
            }
        },
        required: true
    },

    Country_name:{
        type: String,
        validate: {
            validator: function (country: string): boolean {
                return country.length > 2
            }
        },
        required: true
    },

    Password:{
        type: String,
        validate: {
            validator: function (password: string): boolean {
                const pswd1 = /[a-z]/;
                const pswd2 = /[A-Z]/;
                const pswd3 = /[0-9]/;
                const psd1 = pswd1.test(password);
                const psd2 = pswd2.test(password);
                const psd3 = pswd3.test(password);
                return psd1 && psd2 && psd3 && password.length > 8 && password.length <12
            },
            message: "Password length should be 8 to 12 characters long with uppercase, lowercase letters and numbers"
        },
        required: true
    }
});

function convert_lower(User_name){
    if(!(this instanceof mongoose.Document)){
        return User_name
    }
    return User_name.toLowerCase();
}

userSchema.pre<IUser>('validate', function (next) {
    next();
});

export const User = mongoose.model<IUser>('User', userSchema);