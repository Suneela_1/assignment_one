
"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");

var reservationSchema = new mongoose.Schema({

    Car_type:{
        type: String,
        validate:{
            validator: function (car){
                return car.length > 3;
            }
        },
        required: true
    },

    Hours:{
        type: Number,
        validate:{
            validator: function (hours) {
                return hours > 0;
            }
        },
        required: true
    },

    Comments: {
        type: String,
        validate:{
            validator: function (comments){
                return comments.length > 0 && comments.length < 100;
            }
        },
        required: true
    }
});

exports.Reservation = mongoose.model('Reservation', reservationSchema);